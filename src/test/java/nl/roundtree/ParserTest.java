package nl.roundtree;

import nl.roundtree.domain.Direction;
import nl.roundtree.domain.Field;
import nl.roundtree.domain.Robot;
import nl.roundtree.domain.Room;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {

    @Test
    public void testParseRoom() {
        final Room room = Parser.parseRoom("6 5");
        assertThat(room)
                .hasFieldOrPropertyWithValue("width", 6)
                .hasFieldOrPropertyWithValue("depth", 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseRoomInvalidNumberOfArguments() {
        Parser.parseRoom("This is an invalid string");
    }

    @Test(expected = NumberFormatException.class)
    public void testParseRoomInvalidNumbers() {
        Parser.parseRoom("A A");
    }

    @Test
    public void testParseRobot() {
        final Room room = new Room(5, 5);
        final Robot robot = Parser.parseRobot("1 2 N", room);
        assertThat(robot)
                .hasFieldOrPropertyWithValue("room", room)
                .hasFieldOrPropertyWithValue("field", new Field(1, 2))
                .hasFieldOrPropertyWithValue("direction", Direction.N);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseRobotInvalidNumberOfArguments() {
        Parser.parseRobot("Invalid string", new Room(1, 2));
    }

    @Test(expected = NumberFormatException.class)
    public void testParseRobotInvalidNumbers() {
        Parser.parseRobot("A A N", new Room(1, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseRobotInvalidDirection() {
        Parser.parseRobot("1 2 Q", new Room(1, 2));
    }

    @Test(expected = NullPointerException.class)
    public void testParseRobotRoomIsNull() {
        Parser.parseRobot("1 2 N", null);
    }
}