package nl.roundtree.domain;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class RoomTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCreateRoomInvalidWidth() {
        new Room(0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateInvalidDepth() {
        new Room(1, 0);
    }

    @Test
    public void testPositionValid() {
        final Room room = new Room(5, 5);
        assertThat(room.isPositionValid(new Field(0, 0))).isTrue();
        assertThat(room.isPositionValid(new Field(0, 5))).isTrue();
        assertThat(room.isPositionValid(new Field(5, 0))).isTrue();
        assertThat(room.isPositionValid(new Field(5, 5))).isTrue();
    }

    @Test
    public void testPositionNotValid() {
        final Room room = new Room(5, 5);
        assertThat(room.isPositionValid(new Field(-1, 0))).isFalse();
        assertThat(room.isPositionValid(new Field(0, -1))).isFalse();
        assertThat(room.isPositionValid(new Field(6, 5))).isFalse();
        assertThat(room.isPositionValid(new Field(5,6 ))).isFalse();
        assertThat(room.isPositionValid(new Field(6,6 ))).isFalse();
    }

    @Test
    public void testGetNeighbourField() {
        final Room room = new Room(5, 5);
        final Field field = new Field(1, 1);

        final Field neighbourFieldNorth = room.getNeighbourField(field, Direction.N);
        assertThat(neighbourFieldNorth.getX()).isEqualTo(1);
        assertThat(neighbourFieldNorth.getY()).isEqualTo(0);

        final Field neighbourFieldEast = room.getNeighbourField(field, Direction.E);
        assertThat(neighbourFieldEast.getX()).isEqualTo(2);
        assertThat(neighbourFieldEast.getY()).isEqualTo(1);

        final Field neighbourFieldSouth = room.getNeighbourField(field, Direction.S);
        assertThat(neighbourFieldSouth.getX()).isEqualTo(1);
        assertThat(neighbourFieldSouth.getY()).isEqualTo(2);

        final Field neighbourFieldWest = room.getNeighbourField(field, Direction.W);
        assertThat(neighbourFieldWest.getX()).isEqualTo(0);
        assertThat(neighbourFieldWest.getY()).isEqualTo(1);
    }

    @Test
    public void testGetNeighbourFieldOutsideRoom() {
        final Room room = new Room(5, 5);
        final Field field = new Field(5, 5);

        assertThat(room.getNeighbourField(field, Direction.E)).isEqualTo(field);
        assertThat(room.getNeighbourField(field, Direction.S)).isEqualTo(field);
    }

    @Test
    public void testEqualsHashCode() {
        final Room room1 = new Room(5, 5);
        final Room room2 = new Room(5, 5);
        final Room room3 = new Room(6, 5);

        assertThat(room1).isEqualTo(room2);
        assertThat(room2).isNotEqualTo(room3);
        assertThat(room1.hashCode()).isEqualTo(room2.hashCode());
        assertThat(room2.hashCode()).isNotEqualTo(room3.hashCode());
    }
}