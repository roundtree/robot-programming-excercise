package nl.roundtree.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DirectionTest {

    @Test
    public void testGetLeft() {
        assertThat(Direction.N.getLeft()).isEqualTo(Direction.W);
        assertThat(Direction.E.getLeft()).isEqualTo(Direction.N);
        assertThat(Direction.S.getLeft()).isEqualTo(Direction.E);
        assertThat(Direction.W.getLeft()).isEqualTo(Direction.S);
    }

    @Test
    public void testGetRight() {
        assertThat(Direction.N.getRight()).isEqualTo(Direction.E);
        assertThat(Direction.E.getRight()).isEqualTo(Direction.S);
        assertThat(Direction.S.getRight()).isEqualTo(Direction.W);
        assertThat(Direction.W.getRight()).isEqualTo(Direction.N);
    }
}