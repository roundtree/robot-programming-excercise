package nl.roundtree.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FieldTest {

    @Test
    public void testCreateField() {
        final Field field = new Field(10, 20);
        assertThat(field.getX()).isEqualTo(10);
        assertThat(field.getY()).isEqualTo(20);
    }

    @Test
    public void testToString() {
        final Field field = new Field(10, 20);
        assertThat(field.toString()).isEqualTo("10 20");
    }

    @Test
    public void testEqualsHashCode() {
        final Field field1 = new Field(10, 20);
        final Field field2 = new Field(10, 20);
        final Field field3 = new Field(20, 30);

        assertThat(field1).isEqualTo(field2);
        assertThat(field2).isNotEqualTo(field3);
        assertThat(field1.hashCode()).isEqualTo(field2.hashCode());
        assertThat(field2.hashCode()).isNotEqualTo(field3.hashCode());
    }
}