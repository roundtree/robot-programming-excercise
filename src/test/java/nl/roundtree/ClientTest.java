package nl.roundtree;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;


public class ClientTest {

    @Test
    public void testExample1() {
        final InputStream inputStream = new ByteArrayInputStream("5 5\n1 2 N\nRFRFFRFRF".getBytes());
        assertThat(Client.processInput(inputStream)).isEqualTo("Report: 1 3 N");
    }

    @Test
    public void testExample2() {
        final InputStream inputStream = new ByteArrayInputStream("5 5\n0 0 E\nRFLFFLRF".getBytes());
        assertThat(Client.processInput(inputStream)).isEqualTo("Report: 3 1 E");
    }
}