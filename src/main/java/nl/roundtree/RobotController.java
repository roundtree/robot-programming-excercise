package nl.roundtree;

import nl.roundtree.domain.Robot;

class RobotController {

    private final Robot robot;

    RobotController(final Robot robot) {
        this.robot = robot;
    }

    void executeCommand(final char command) {
        if ('R' == command) {
            robot.rotateRight();
        } else if ('L' == command) {
            robot.rotateLeft();
        } else if ('F' == command) {
            robot.moveForward();
        } else {
            throw new IllegalArgumentException(String.format("I can only move forward (F), rotate left (L) or rotate right (R). I don't know what '%s' means", command));
        }
    }
}
